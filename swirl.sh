#!/bin/bash

for i in `seq 1 100`
do
    
    echo -ne '\';
    sleep 0.1;
    echo -ne '\b-';
    sleep 0.1;
    echo -ne '\b/'
    sleep 0.1;
    echo -ne '\b|';
    sleep 0.1;
    echo -ne '\b\';
    sleep 0.1;
    echo -ne '\b-';
    sleep 0.1;
    echo -ne '\b/';
    sleep 0.1;
    echo -ne '\b|';
    echo -ne '\b'
done

echo -ne "\b \n";
